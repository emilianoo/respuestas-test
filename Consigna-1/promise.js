//callback

function buscarEnFacebook(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error);
  } else {
    callback(null, result.data);
  }
}

function buscarEnGithub(texto, callback) {
  /* Hace algunas cosas y las guarda en "result" */
  if (result.error) {
    callback(error, result.error);
  } else {
    callback(null, result.data);
  }
}

//promise
function buscarEnFacebook(texto) {
  return new Promise((resolve, reject) => {
    if (result.error) {
      resolve(error, result.error);
    } else {
      reject(null, result.error);
    }
  });
}

function buscarEnGithub(texto) {
  return new Promise((resolve, reject) => {
    if (result.error) {
      resolve(error, result.error);
    } else {
      reject(null, result.error);
    }
  });
}
