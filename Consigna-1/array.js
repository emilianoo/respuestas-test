//Partiendo del siguiente array:

var endorsements = [
  { skill: 'css', user: 'Bill' },
  { skill: 'javascript', user: 'Chad' },
  { skill: 'javascript', user: 'Bill' },
  { skill: 'css', user: 'Sue' },
  { skill: 'javascript', user: 'Sue' },
  { skill: 'html', user: 'Sue' }
];



//Ordenado como el ejemplo de abajo

var endorsements = [
  { skill: 'css', user: ['Bill', 'Sue'] },
  { skill: 'javascript', user: ['Chad', 'Bill', 'Sue'] },
  { skill: 'html', user: ['Sue'] }
]



//¿Cómo podrías ordenarlo de la siguiente forma?:

[
  { skill: 'css', users: [ 'Bill', 'Sue', 'Sue' ], count: 2 },
  { skill: 'javascript', users: [ 'Chad', 'Bill', 'Sue' ], count: 3 },
  { skill: 'html', users: [ 'Sue' ], count: 1 }
]