var Foo = function( a ) {
    var baz = function() {
      return a;
    };
    this.baz = function() {
      return a;
    };
  };

  Foo.prototype = {
    biz: function() {
      return a;
    }
  };

  var f = new Foo( 7 );
  f.bar(); 
  f.baz(); 
  f.biz(); 


  // f.bar(); is not a function
  // f.baz(); 7
  // f.biz(); a is not defined