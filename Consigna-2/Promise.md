 a. What is a Promise?
    El objeto Promise es usado para computaciones asíncronas.
    Una promesa representa un valor que puede estar disponible ahora,
    en el futuro, o nunca.

Ej...

var p1 = new Promise(function(resolve, reject) {
  resolve('Success!');
  // or
  // reject ("Error!");
});

p1.then(function(value) {
  console.log(value); // Success!
}, function(reason) {
  console.log(reason); // Error!
});