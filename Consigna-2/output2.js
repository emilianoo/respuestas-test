// d. What will be the output when the following code is executed? Explain.


console.log(false == "0"); // true
console.log(false === "0"); // false


// El == operador está comprobando los valores de los dos objetos y regresando true
// Pero === está viendo que no son del mismo tipo y regresan false