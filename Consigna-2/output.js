// What will the code below output to the console and why?

var myObject = {
  foo: "bar",
  func: function() {
    var self = this;
    console.log("outer func: this.foo = " + this.foo);
    console.log("outer func: self.foo = " + self.foo);
    (function() {
      console.log("inner func: this.foo = " + this.foo);
      console.log("inner func: self.foo = " + self.foo);
    })();
  }
};

myObject.func();

//Output to console

this.foo = bar //outer func: 
self.foo = bar  //outer func: 
this.foo = undefined //inner func: 
self.foo = bar //inner func: 


// En outer func this y self hacen refieren a myObject ambos pueden referenciarse y acceder a Foo.
// En inner func this no hace referencia a myObject. Entonces this.foo es undefined 
// mientras que self hace referencia a la variable local, mantiene su alcance y accede.
