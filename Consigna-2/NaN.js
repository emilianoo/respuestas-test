// c. What is NaN? What is its type? How can you reliably test if a value is equal to NaN ?
    // NaN es una propiedad del objeto global .
    // El valor inicial de NaN es (Not-A-Number),
    // el mismo que el valor de Number.NaN.
    // En los navegadores modernos,
    // NaN es una propiedad no configurable y no modificable.

    // NaN compara desigual (a través de ==, !=, ===, y !==) en cualquier otro valor - incluyendo a otro valor NaN.
    // Number.isNaN() o isNaN() determina con mayor claridad si un valor es NaN.

NaN === NaN; // false
Number.NaN === NaN; // false
isNaN(NaN); // true
isNaN(Number.NaN); // true

function valueIsNaN(v) {
return v !== v;
}

valueIsNaN(1); // false
valueIsNaN(NaN); // true
valueIsNaN(Number.NaN); // true